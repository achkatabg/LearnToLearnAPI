﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LearnToLearn.Models
{
    public class User : BaseModel
    {
        [Required]
        public string Name { get; set; }

        [Required]
        [EmailAddress]
        [Index(IsUnique = true)]
        [StringLength(30)]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public bool IsTeacher { get; set; }

        public virtual List<Course> Courses { get; set; }
        public virtual List<Enrollment> Enrollments { get; set; }

        public User()
        {
            Courses = new List<Course>();
            Enrollments = new List<Enrollment>();
        }
    }
}