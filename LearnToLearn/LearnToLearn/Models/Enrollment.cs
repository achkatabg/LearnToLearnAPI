﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LearnToLearn.Models
{
    public class Enrollment : BaseModel
    {
        public int UserID { get; set; }

        public int CourseID { get; set; }

        [Required]
        public double Grade { get; set; } = 0.00;

        [Required]
        public DateTime CreatedAt { get; set; }

        [Required]
        public DateTime UpdatedAt { get; set; }

        public virtual User User { get; set; }
        public virtual Course Course { get; set; }
       
    }
}