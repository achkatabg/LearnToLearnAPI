﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LearnToLearn.Models
{
    public class Course : BaseModel
    {
        [Required]
        [Index(IsUnique =true)]
        [StringLength(20)]
        public string Name { get; set; }

        public string Description { get; set; }

        [ForeignKey("User")]
        public int? TeacherId { get; set; }

        public bool IsVisible { get; set; }

        [Required]
        public int Capacity { get; set; }

        [Required]
        public DateTime CreatedAt { get; set; } = DateTime.Now;

        [Required]
        public DateTime UpdatedAt { get; set; } = DateTime.Now;

        public virtual User User { get; set; }
        public virtual List<Enrollment> Enrollments { get; set; }

        public Course()
        {
            Enrollments = new List<Enrollment>();
        }

    }
}