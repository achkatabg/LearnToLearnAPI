﻿using LearnToLearn.BindModels;
using LearnToLearn.BLL;
using LearnToLearn.Models;
using LearnToLearn.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LearnToLearn.Controllers
{

    [Authorize]
    public class EnrollmentController : ApiController
    {
        private EnrollmentService enrollmentService;
        public EnrollmentController()
        {
            UnitOfWork unitOfWork = new UnitOfWork();
            this.enrollmentService = new EnrollmentService(new ValidationDictionary(ModelState), unitOfWork);
        }

        [HttpPost]
        public IHttpActionResult EnrollToCourse(EnrollBindModel bindModel)
        {
            Enrollment enroll = new Enrollment {
                UserID = bindModel.UserID,
                CourseID = bindModel.CourseID,
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now
            };

            if (enrollmentService.SaveEnrollment(enroll))
            {
                return BadRequest();
            }

            return Ok(bindModel);
        }
        [Authorize(Roles ="Teacher")]
        [HttpPatch]
        public IHttpActionResult SetGrade(int id,[FromBody] EnrollGradeBindModel bindModel)
        {
            Enrollment enroll = enrollmentService.GetEnrollmentById(id);
            if(enrollmentService.DBEnrollmentValidate(enroll))
            {
                enroll.Grade = bindModel.Grade;
                enroll.UpdatedAt = DateTime.Now;

                enrollmentService.SaveEnrollment(enroll);
            }
            else
            {
                return Content(HttpStatusCode.BadRequest,"Invalid EnrollmentID or grade range ...");
            }

            return Ok("Successfully rated !");
        }
    }
}
