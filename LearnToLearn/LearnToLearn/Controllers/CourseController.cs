﻿using LearnToLearn.BindModels;
using LearnToLearn.BLL;
using LearnToLearn.Models;
using LearnToLearn.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LearnToLearn.Controllers
{
    [Authorize(Roles ="Teacher")]
    public class CourseController : ApiController
    {
        private CourseService courseService;
        public CourseController()
        {
            UnitOfWork unitOfWork = new UnitOfWork();
            this.courseService = new CourseService(new ValidationDictionary(ModelState), unitOfWork);
        }

        #region Crud logic courses
        [HttpGet]
        public IHttpActionResult GetAllCourses()
        {
            List<Course> courses = courseService.GetAllCourses();
            if (!courseService.DBNoRecordsValidatation(courses))
            {
                return Content(HttpStatusCode.NotFound,"No records found !");
            }
            else {
                List<CourseBindModel> model = new List<CourseBindModel>();
                foreach (Course course in courses)
                {
                    CourseBindModel modelItem = new CourseBindModel(course);
                    model.Add(modelItem);
                }
                return Ok(model);
            }
        }

        [HttpPost]
        public IHttpActionResult Post(CreateCourseBindModel bindModel)
        {
            Course dbCourse = new Course
            {
                Name = bindModel.Name,
                TeacherId = bindModel.TeacherID,
                Capacity = bindModel.Capacity,
                Description = bindModel.Description
            };

            if (courseService.SaveCourse(dbCourse))
            {
                return BadRequest();
            }

            return Ok(bindModel);
        }

        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            Course course = courseService.GetCourseById(id);
            bool IsDeleted = courseService.DeleteUserById(id);

            if(IsDeleted)
            {
                return Ok("Deleted!");
            }
            else
            {
                return Content(HttpStatusCode.NotFound, "This course doesn't exists !");
            }
        }

        [HttpPatch]
        public IHttpActionResult CourseMark(int id, [FromBody] CourseMarkBindModel bindModel)
        {
            Course course = courseService.GetCourseById(id);
            if (courseService.DBCourseValidate(course))
            {
                course.IsVisible = bindModel.IsVisible;
                courseService.SaveCourse(course);
            }
            else
            {
                return Content(HttpStatusCode.NotFound, "Invalid ID");
            }

            return Ok("Changed successfully!");
        }
        
        [HttpPut]
        public IHttpActionResult Update(int id,CourseEditBindModel bindModel)
        {
            Course course = courseService.GetCourseById(id);
            if(courseService.DBCourseValidate(course))
            {
                course.Name = bindModel.Name;
                course.TeacherId = bindModel.TeacherId;
                course.Capacity = bindModel.Capacity;
                course.Description = bindModel.Description;
                course.UpdatedAt = bindModel.UpdatedAt;

                courseService.SaveCourse(course);
            }
            else
            {
                return Content(HttpStatusCode.NotFound, "Course with this id doesen't exists !");
            }
            return Ok("Updated successfully !");
        }
        
    }
        #endregion
}
