﻿using LearnToLearn.Models;
using LearnToLearn.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LearnToLearn.BLL
{
    public class EnrollmentService
    {
        private UnitOfWork unitOfWork;
        private ValidationDictionary validDictionary;

        public EnrollmentService(ValidationDictionary validationDictionary, UnitOfWork unitOfWork)
        {
            this.validDictionary = validationDictionary;
            this.unitOfWork = unitOfWork;
        }

        public Enrollment GetEnrollmentById(int id)
        {
            return unitOfWork.EnrollmentRepository.GetById(id);
        }

        public bool DBEnrollmentValidate(Enrollment enroll)
        {
            if (enroll != null)
            {
                return this.validDictionary.IsValid;
            }
            return false;
        }

        public bool SaveEnrollment(Enrollment enroll)
        {
            unitOfWork.EnrollmentRepository.Save(enroll);
            return unitOfWork.Save() > 0;
        }
    }
}