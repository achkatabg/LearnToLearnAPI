﻿using LearnToLearn.Models;
using LearnToLearn.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LearnToLearn.BLL
{
    public class CourseService
    {
        private UnitOfWork unitOfWork;
        private ValidationDictionary validDictionary;

        public CourseService(ValidationDictionary validationDictionary, UnitOfWork unitOfWork)
        {
            this.validDictionary = validationDictionary;
            this.unitOfWork = unitOfWork;
        }

        public List<Course> GetAllCourses()
        {
            return unitOfWork.CourseRepository.GetAll();
        }

        public Course GetCourseById(int id)
        {
            return unitOfWork.CourseRepository.GetById(id);
        }

        public bool DeleteUserById(int id)
        {
            return unitOfWork.CourseRepository.DeleteById(id);
        }

        public bool DBCourseValidate(Course course)
        {
            if(course!=null)
            {
                return this.validDictionary.IsValid;
            }
            return false;
        }

        public bool DBNoRecordsValidatation (List<Course> courses)
        {
            if(courses.Count<=0)
            {
                return false;
            }
            return true;
        }
        public bool SaveCourse(Course course)
        {
            unitOfWork.CourseRepository.Save(course);
            return unitOfWork.Save() > 0;
        }
    }
}