﻿using LearnToLearn.Models;
using LearnToLearn.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LearnToLearn.BLL
{
    public class UserService
    { 
        private UnitOfWork unitOfWork;
       
        public UserService(UnitOfWork unitOfWork)
        { 
            this.unitOfWork = unitOfWork;
        }

        public User GetUserByCredentials(string email,string password)
        {
            return unitOfWork.UserRepository.GetUserByEmailAndPassword(email, password);
        }
    }
}