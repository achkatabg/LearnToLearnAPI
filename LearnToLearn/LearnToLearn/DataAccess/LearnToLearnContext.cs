﻿using LearnToLearn.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace LearnToLearn.DataAccess
{
    public class LearnToLearnContext : DbContext
    {
        public LearnToLearnContext() : base("LearnToLearnDB")
        {

        }

        public DbSet<User> Users { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DbSet<Enrollment> Enrollments { get; set; }
    }
}