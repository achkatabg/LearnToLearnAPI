﻿using LearnToLearn.DataAccess;
using LearnToLearn.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LearnToLearn.Repository
{
    public class EnrollmentRepository : BaseRepository<Enrollment>
    {
        public EnrollmentRepository(LearnToLearnContext context)
        {
            this.Context = context;
        }
    }
}