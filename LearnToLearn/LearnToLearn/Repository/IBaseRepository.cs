﻿using LearnToLearn.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearnToLearn.Repository
{
    public interface IBaseRepository<T> where T : BaseModel
    {
        List<T> GetAll(Func<T, bool> filter = null);

        T GetById(int id);

        void Save(T item);

        void Create(T item);

        void Update(T item, Func<T, bool> findByPredicate);
    }
}
