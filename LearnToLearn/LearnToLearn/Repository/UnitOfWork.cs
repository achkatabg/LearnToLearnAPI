﻿using LearnToLearn.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LearnToLearn.Repository
{
    public class UnitOfWork
    {
        private LearnToLearnContext Context = new LearnToLearnContext();

        private CourseRepository courseRepository;
        private EnrollmentRepository enrollmentRepository;
        private UserRepository userRepository;

        public CourseRepository CourseRepository
        {
            get
            {
                if (this.courseRepository == null)
                {
                    this.courseRepository = new CourseRepository(Context);
                }
                return courseRepository;
            }
        }

        public EnrollmentRepository EnrollmentRepository
        {
            get
            {
                if (this.enrollmentRepository == null)
                {
                    this.enrollmentRepository = new EnrollmentRepository(Context);
                }
                return enrollmentRepository;
            }
        }

        public UserRepository UserRepository
        {
            get
            {
                if (this.userRepository == null)
                {
                    this.userRepository = new UserRepository(Context);
                }
                return userRepository;
            }
        }

        public int Save()
        {
            return Context.SaveChanges();
        }
    }
}