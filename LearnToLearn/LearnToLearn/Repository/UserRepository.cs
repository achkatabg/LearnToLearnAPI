﻿using LearnToLearn.DataAccess;
using LearnToLearn.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LearnToLearn.Repository
{
    public class UserRepository : BaseRepository<User>
    {
        public UserRepository(LearnToLearnContext context)
        {
            this.Context = context;
        }

        public User GetUserByEmailAndPassword(string email, string password)
        {
            User user = base.DBSet.FirstOrDefault(u => u.Email == email && u.Password == password);
            if (user != null)
            {
                user.Password = string.Empty;
            }
            return user;
        }
    }
}