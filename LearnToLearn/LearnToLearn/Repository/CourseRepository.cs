﻿using LearnToLearn.DataAccess;
using LearnToLearn.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LearnToLearn.Repository
{
    public class CourseRepository : BaseRepository<Course>
    {
        public CourseRepository(LearnToLearnContext context)
        {
            this.Context = context; 
        }
    }
}