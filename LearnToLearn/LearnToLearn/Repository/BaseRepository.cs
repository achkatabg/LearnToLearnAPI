﻿using LearnToLearn.DataAccess;
using LearnToLearn.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace LearnToLearn.Repository
{
    public abstract class BaseRepository<T> : IBaseRepository<T>
       where T : BaseModel
    {
        protected LearnToLearnContext Context;

        public BaseRepository()
        {
            Context = new LearnToLearnContext();
        }

        protected DbSet<T> DBSet
        {
            get
            {
                return Context.Set<T>();
            }
        }

        public List<T> GetAll(Func<T, bool> filter = null)
        {
            if (filter != null)
            {
                return Context.Set<T>().Where(filter).ToList();
            }

            return Context.Set<T>().ToList();
        }

        public T GetById(int id)
        {
            return Context.Set<T>().Find(id);
        }

        public void Create(T item)
        {
            Context.Set<T>().Add(item);
            Context.SaveChanges();
        }

        public void Update(T item, Func<T, bool> findByPredicate)
        {
            var local = Context.Set<T>()
                         .Local
                         .FirstOrDefault(findByPredicate);

            Context.Entry(item).State = EntityState.Modified;
            Context.SaveChanges();
        }

        public bool DeleteById(int id)
        {
            bool isDeleted = false;
            T dbItem = Context.Set<T>().Find(id);
            if (dbItem != null)
            {
                Context.Set<T>().Remove(dbItem);
                int recordsChanged = Context.SaveChanges();
                isDeleted = recordsChanged > 0;
            }
            return isDeleted;
        }

        public virtual void Save(T item)
        {
            if (item.Id == 0)
            {
                Create(item);
            }
            else
            {
                Update(item, x => x.Id == item.Id);
            }
        }

    }
}