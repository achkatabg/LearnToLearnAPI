﻿using LearnToLearn.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LearnToLearn.BindModels
{
    public class EnrollGradeBindModel
    {
        [Range(2.00,6.00,ErrorMessage ="Grade must be in 2 - 6 range !")]
        public double Grade { get; set; }
    }
}