﻿using LearnToLearn.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LearnToLearn.BindModels
{
    public class CourseEditBindModel
    {
        public string Name { get; set; }
        public int? TeacherId { get; set; }
        public int Capacity { get; set; }
        public string Description { get; set; }
        public DateTime UpdatedAt { get; set; } = DateTime.Now;

        public CourseEditBindModel()
        {
                
        }

        public CourseEditBindModel(Course course)
        {
            this.Name = course.Name;
            this.TeacherId = course.TeacherId;
            this.Capacity = course.Capacity;
            this.Description = course.Description;
            this.UpdatedAt = course.UpdatedAt;
        }
    }
}