﻿using LearnToLearn.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LearnToLearn.BindModels
{
    public class CreateCourseBindModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int? TeacherID { get; set; }
        public int Capacity { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

        public CreateCourseBindModel()
        {

        }

        public CreateCourseBindModel(Course course)
        {
            this.Name = course.Name;
            this.Description = course.Description;
            this.TeacherID = course.TeacherId;
            this.Capacity = course.Capacity;
            this.CreatedAt = course.CreatedAt;
            this.UpdatedAt = course.UpdatedAt;
        }
    }
    
}