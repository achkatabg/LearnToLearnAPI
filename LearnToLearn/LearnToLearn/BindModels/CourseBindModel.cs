﻿using LearnToLearn.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LearnToLearn.BindModels
{
    public class CourseBindModel
    {
        public int Id { get; set; }
        public int? TeacherID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Capacity { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

        public CourseBindModel()
        {

        }

        public CourseBindModel(Course course)
        {
            this.Id = course.Id;
            this.TeacherID = course.TeacherId;
            this.Name = course.Name;
            this.Description = course.Description;
            this.Capacity = course.Capacity;
            this.CreatedAt = course.CreatedAt;
            this.UpdatedAt = course.UpdatedAt;
        }
    }
}