﻿using LearnToLearn.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LearnToLearn.BindModels
{
    public class EnrollBindModel
    {
       public int UserID { get; set; }
       public int CourseID { get; set; }
       public double Grade { get; set; }

       public EnrollBindModel()
       {

       }

       public EnrollBindModel(Enrollment enroll)
       {
            this.UserID = enroll.UserID;
            this.CourseID = enroll.CourseID;
            this.Grade = enroll.Grade;
       }
    }
}